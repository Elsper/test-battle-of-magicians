using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public bool _player;

    public bool Player
    {
        set
        {
            _player = value;
        }
    }

    public virtual void Awake()
    {
        gameObject.name = "Shield";
    }

    public bool IsSameOwner(bool owner)
    {
        return _player == owner ? true : false;
    }

    private void OnDestroy()
    {
        Destroy(gameObject);
    }

}
