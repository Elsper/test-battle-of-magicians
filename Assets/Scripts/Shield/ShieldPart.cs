using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldPart : Shield
{

    [SerializeField] private RectTransform _rt;
    [SerializeField] private MeshCollider _meshCollider;

    private Vector3 _currentEulerAngles = new Vector3(90, 0, 0);
    private float _speed;

    private void Update()
    {
        _currentEulerAngles += new Vector3(0, 25, 0) * Time.deltaTime * _speed;
        _rt.eulerAngles = _currentEulerAngles;
    }

    public override void Awake()
    {
        base.Awake();
        if (_rt == null) _rt = GetComponent<RectTransform>();
        if (_meshCollider == null) _meshCollider = GetComponent<MeshCollider>();
    }

    public void MakeShield(float speed, float distance, float position, bool player)
    {
        _speed = speed;
        Player = player;

        Mesh mesh = gameObject.AddComponent<MeshFilter>().mesh;

        List<Vector3> newVertices = new List<Vector3>();
        List<int> newTriangles = new List<int>();
        List<Vector2> newUV = new List<Vector2>();

        int sectorsMesh = 8;

        newVertices.Add(new Vector3(-sectorsMesh / 2, distance, 0));
        newVertices.Add(new Vector3(-sectorsMesh / 2, distance, 20));
        newUV.Add(new Vector2(0, 0));
        newUV.Add(new Vector2(0, 1));
        for (int i = 1; i < sectorsMesh; i++)
        {
            newVertices.Add(new Vector3(i - sectorsMesh / 2, distance + Mathf.Sin(180f / (sectorsMesh - 1) * i * Mathf.Deg2Rad), 0));
            newVertices.Add(new Vector3(i - sectorsMesh / 2, distance + Mathf.Sin(180f / (sectorsMesh - 1) * i * Mathf.Deg2Rad), 20));
            newUV.Add(new Vector2((float)i / sectorsMesh, 0));
            newUV.Add(new Vector2((float)i / sectorsMesh, 1));

            newTriangles.Add(i * 2 - 2);
            newTriangles.Add(newVertices.Count - 2);
            newTriangles.Add(newVertices.Count - 1);
            newTriangles.Add(i * 2 - 1);
            newTriangles.Add(i * 2 - 2);
            newTriangles.Add(newVertices.Count - 1);
        }

        mesh.Clear();
        mesh.vertices = newVertices.ToArray();
        mesh.triangles = newTriangles.ToArray();
        mesh.uv = newUV.ToArray();
        mesh.Optimize();
        mesh.RecalculateNormals();

        mesh.RecalculateBounds();

        _meshCollider.sharedMesh = mesh;
    }
}
