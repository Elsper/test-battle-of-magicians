using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public abstract class Wizard : MonoBehaviour
{
    [SerializeField] private HealthProgressBar _healtProgressBar;
    [SerializeField] private ProgressBar _shieldProgressBar;
    [SerializeField] private Weapons _weapons;
    [SerializeField] private GameObject _shieldPrefab;
    [SerializeField] private Animator _animator;
    [SerializeField] private RectTransform _rtCenterPoint;
    [SerializeField] private FullShield _fullShield;
    [SerializeField] private ProgressBar _fillImageStaff;
    [SerializeField] private bool _isPlayer;

    private bool _readyToFight = false;
    private bool _readyToShoot = false;
    private bool _readyShield = false;

    private List<Shield> _shields = new List<Shield>();
    private Tween _healthTween;

    public Weapons Weapons { get { return _weapons; } }
    public bool SetIsPLayer { set { _isPlayer = value; } }
    public RectTransform RtCenterPoint { get { return _rtCenterPoint; } }

    public void Init()
    {
        if (_shieldProgressBar != null)
        {
            _shieldProgressBar.OnFullProgress += OnReadyShield;
            _shieldProgressBar.MakeAutoRegen(0.25f);
        }

        _fillImageStaff.OnFullProgress += OnReadyToShoot;
        _fillImageStaff.MakeAutoRegen(1);

        _weapons.SetWeapon(1, _isPlayer);

        Game.Instance.LevelsManager.OnEndLevel += SetDefault;
        Game.Instance.LevelsManager.OnStartLevel += WakeUpWhenStartLevel;
    }
    private void OnDestroy()
    {
        Game.Instance.LevelsManager.OnEndLevel -= SetDefault;
    }
    private void SetDefault()
    {
        _readyToFight = false;
        _healthTween.Kill();
        _healtProgressBar.CurrentProgress = 0.1f;
        StartAnimation("Die");

        if (_shields.Count > 0)
            for (int i = _shields.Count - 1; i >= 0; i--)
            {
                Destroy(_shields[i]);
            }
    }

    public abstract void WakeUpWhenStartLevel(int level);

    protected void WakeUP(float speedWeaponRecharge, int countShields)
    {
        _fillImageStaff.MakeAutoRegen(speedWeaponRecharge);

        _healtProgressBar.CurrentProgress = 0.1f;
        _healthTween = DOTween.To(() => _healtProgressBar.CurrentProgress, h => _healtProgressBar.CurrentProgress = h, _healtProgressBar.MaxProgress, 2)
            .SetDelay(1)
            .OnComplete(SetNowReadyToFight);

        StartAnimation("DieRecovery");

        if (countShields > 0)
            MakeShield(1, 5, 0);
        if (countShields > 1)
            MakeShield(2, 5.5f, 0);
        if (countShields > 2)
            MakeShield(2.5f, 6f, 0);
    }

    private void SetNowReadyToFight()
    {
        _readyToFight = true;
        if (_readyToShoot)
            OnReadyToShoot();
    }
    private void StartAnimation(string animation)
    {
        _animator.Play(animation, 0);
    }

    private void MakeShield(float speed, float distance, float pos)
    {
        GameObject newGOShield = Instantiate(_shieldPrefab, gameObject.transform);
        ShieldPart newShield = newGOShield.GetComponent<ShieldPart>();
        newShield.MakeShield(speed, distance, pos, _isPlayer);
        _shields.Add(newShield);
    }

    public void TakeDamage(float damage)
    {
        if (!_readyToFight) return;

        StartAnimation("DefendHit");
        float currentHealth = _healtProgressBar.MakeDamage(damage);
        if (currentHealth <= 0)
        {
            Game.Instance.LevelsManager.EndLevel();

            if (_isPlayer)
                Game.Instance.UIManager.ShowDeathUi();
            else
                Game.Instance.UIManager.ShowWinUi();
        }
    }

    public void TraceAttack(Vector3 target, Vector3 curve = default(Vector3))
    {
        if (!_readyToFight) return;
        if (!_readyToShoot) return;
        Weapons.MakeTraceAttack(target, curve);

        if (_isPlayer) Weapons.ShowTraceLine();
    }
    public void Attack(Vector3 target = default(Vector3))
    {
        if (!_readyToFight) return;
        if (!_readyToShoot) return;

        _readyToShoot = false;
        _fillImageStaff.CurrentProgress = 0;
        StartAnimation("Attack01");
        Weapons.MakeAttack();
    }
    public void HideTrace()
    {
        Weapons.HideTrace();
    }

    public void TryUseFullShield()
    {
        if (!_readyShield) return;

        _readyShield = false;
        _shieldProgressBar.CurrentProgress = 0;
        _fullShield.gameObject.SetActive(true);
        _fullShield.Player = _isPlayer;
        Invoke("OffFullShield", 0.5f);
    }

    public virtual void OnReadyShield()
    {
        _readyShield = true;
    }

    public virtual void OnReadyToShoot()
    {
        _readyToShoot = true;
    }
}
