using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;

public class Weapons : MonoBehaviour
{
    [SerializeField] private Transform[] _shooterPoints;
    [SerializeField] private GameObject[] _staffs;
    [SerializeField] private GameObject[] _spheres;

    private bool _isPlayerWeapon = true;
    private int _nomerWeapon = 0;

    private Vector3[] _bezierPoints = null;
    private List<Vector3> _listLineVector3 = new List<Vector3>();
    private GameObject _line_game_object;
    private LineRenderer _line;

    public void SetWeapon(int nomer, bool isPlayerWeapon)
    {
        _isPlayerWeapon = isPlayerWeapon;

        _nomerWeapon = nomer;
        if (_nomerWeapon < _staffs.Length)
            for (int i = 0; i < _staffs.Length; i++)
            {
                if (i == _nomerWeapon)
                    _staffs[i].SetActive(true);
                else
                    _staffs[i].SetActive(false);
            }
    }

    public Transform GetShooterPoint()
    {
        return _shooterPoints[_nomerWeapon];
    }

    public void MakeTraceAttack(Vector3 target, Vector3 curve = default(Vector3))
    {
        _listLineVector3 = new List<Vector3>();
        _listLineVector3.Add(_shooterPoints[_nomerWeapon].position);

        if (curve != default(Vector3))
        {
            _bezierPoints = BezierUtils.MakeBezierPoints(_shooterPoints[_nomerWeapon].position, target, curve, curve, 10);
            _listLineVector3.AddRange(_bezierPoints.ToList());
        }
        else
            _listLineVector3.Add(target);

        _shooterPoints[_nomerWeapon].rotation = Quaternion.LookRotation(target - _shooterPoints[_nomerWeapon].position);

        RaycastHit hit;
        if (Physics.Raycast(_shooterPoints[_nomerWeapon].position, target - _shooterPoints[_nomerWeapon].position, out hit, 100))
        {
            if (hit.transform.name == "Plane")
            {
                _listLineVector3.Add(hit.point);
                //Old real reflect
                //_listLineVector3.Add(hit.point+Vector3.Reflect((hit.point- _shooterPoints[nomerWeapon].position), (hit.normal.normalized)));

                //New fake reflect
                _listLineVector3.Add(Game.Instance.BattleController.GetCenterTargetWizard(!_isPlayerWeapon));
            }
        }
        else
            _listLineVector3.Add(target);


    }
    public void ShowTraceLine()
    {
        if (_line_game_object == null)
        {
            _line_game_object = new GameObject(name);
            _line_game_object.transform.SetParent(gameObject.transform, false);
        }
        _line_game_object.SetActive(true);

        if (_line == null)
        {
            _line = _line_game_object.AddComponent<LineRenderer>();
            _line.startWidth = 0.1f;
            _line.endWidth = 0.1f;
        }



        _line.positionCount = _listLineVector3.Count;
        _line.SetPositions(_listLineVector3.ToArray());
    }
    public void HideTrace()
    {
        if (_line_game_object != null)
            _line_game_object.SetActive(false);
    }

    public void MakeAttack()
    {
        GameObject newSphere = Instantiate(_spheres[0]);
        newSphere.transform.position = _shooterPoints[_nomerWeapon].position;

        newSphere.GetComponent<Sphere>().StartShoot(20, 20, _listLineVector3, _isPlayerWeapon);
    }


}
