using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Wizard
{

    public override void WakeUpWhenStartLevel(int level)
    {
        switch (level)
        {
            case 1:
            default:
                WakeUP(0.2f, 0);
                break;
            case 2:
                WakeUP(0.3f, 1);
                break;
            case 3:
                WakeUP(0.5f, 2);
                break;
            case 4:
                WakeUP(0.7f, 2);
                break;
            case 5:
                WakeUP(1f, 3);
                break;
            case 6:
                WakeUP(1.5f, 3);
                break;
            case 7:
                WakeUP(2f, 3);
                break;
            case 8:
                WakeUP(2.5f, 3);
                break;
        }
    }

    public override void OnReadyToShoot()
    {
        base.OnReadyToShoot();
        TraceAttack(Game.Instance.BattleController.GetCenterTargetWizard(true));
        Attack();
    }
}
