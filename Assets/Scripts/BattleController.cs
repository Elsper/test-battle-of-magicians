using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleController : IUpdate
{
    private Vector3 _keepClickTarget;

    public Vector3 GetCenterTargetWizard(bool player)
    {
        return player ? Game.Instance.Player.RtCenterPoint.position :
                        Game.Instance.Enemy.RtCenterPoint.position;
    }


    void IUpdate.Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (_keepClickTarget == default(Vector3))
            {
                _keepClickTarget = ray.GetPoint(60);
            }
        }
        else
        if (Input.GetMouseButtonUp(0))
        {
            Game.Instance.Player.HideTrace();
            Game.Instance.Player.Attack(_keepClickTarget);
            _keepClickTarget = default(Vector3);
        }
        else
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Game.Instance.Player.TraceAttack(_keepClickTarget, ray.GetPoint(45));
        }
        else
        if (Input.GetMouseButton(1))
        {
            Game.Instance.Player.TryUseFullShield();
        }

    }

}
