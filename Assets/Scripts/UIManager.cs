using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : MonoBehaviour
{
    [Header("Main menu")]
    [SerializeField] private GameObject _startUI;
    [SerializeField] private GameObject _againUI;
    [SerializeField] private DeathUI _deathUI;
    [SerializeField] private GameObject _levelTextGO;
    [SerializeField] private TextMeshProUGUI _levelText;
    [SerializeField] private GameObject _explanationTextGO;
    [SerializeField] private Button _buttonStart;
    [SerializeField] private Button _buttonAgain;

    [Header("Load")]
    [SerializeField] private TextMeshProUGUI _loadedText;
    [SerializeField] private RectTransform _rtLoadedText;
    [SerializeField] private Button _buttonLoad;

    [Header("Next")]
    [SerializeField] private Button _buttonNext;
    [SerializeField] private TextMeshProUGUI _nextLevelText;

    [Header("Win")]
    [SerializeField] private GameObject _winUI;
    [SerializeField] private TextMeshProUGUI _orText;
    [SerializeField] private GameObject _bonusPanelGO;
    [SerializeField] private Button _buttonAddShield;
    [SerializeField] private Button _buttonAddSpeed;



    public void Init()
    {
        _buttonLoad.gameObject.SetActive(SaveLoadManager.CheckExistLoadingFile());
        _buttonLoad.onClick.AddListener(SaveLoadManager.MakeLoad);
        _buttonStart.onClick.AddListener(StartLevel);
        _buttonAgain.onClick.AddListener(StartLevel);

        _buttonNext.onClick.AddListener(StartLevel);

        _buttonAddShield.onClick.AddListener(OnClickAddShield);
        _buttonAddSpeed.onClick.AddListener(OnClickAddSpeed);

        _loadedText.gameObject.SetActive(false);

        ShowStartUI();
    }

    private void OnDestroy()
    {
        _buttonLoad.onClick.RemoveAllListeners();
        _buttonStart.onClick.RemoveAllListeners();
        _buttonAgain.onClick.RemoveAllListeners();
        _buttonNext.onClick.RemoveAllListeners();
        _buttonAddShield.onClick.RemoveAllListeners();
        _buttonAddSpeed.onClick.RemoveAllListeners();
    }

    private void HideMainMenu()
    {
        _explanationTextGO.SetActive(false);
        _levelTextGO.SetActive(false);
        _againUI.SetActive(false);
        _startUI.SetActive(false);
        _winUI.SetActive(false);
    }

    public void GameWasLoaded()
    {
        _rtLoadedText.DOKill();
        _rtLoadedText.anchoredPosition = new Vector2(0, 50);
        _loadedText.gameObject.SetActive(true);
        _rtLoadedText.DOAnchorPosY(500, 2).OnComplete(() => { _loadedText.gameObject.SetActive(false); });

        ShowStartUI();
    }
    private void ShowStartUI()
    {
        ShowLevelTexts();
        _startUI.SetActive(true);
        _winUI.SetActive(false);
        _againUI.SetActive(false);
    }
    public void ShowAgainUi()
    {
        ShowLevelTexts();
        _deathUI.gameObject.SetActive(false);
        _againUI.SetActive(true);
    }
    public void ShowDeathUi()
    {
        _deathUI.ShowDeathScreen();
    }

    private void OnClickAddShield()
    {
        Game.Instance.Player.PlayerCountShields++;

        _nextLevelText.gameObject.SetActive(true);
        _buttonNext.gameObject.SetActive(true);
        _bonusPanelGO.gameObject.SetActive(false);

        SaveLoadManager.MakeSave();
    }
    private void OnClickAddSpeed()
    {
        Game.Instance.Player.PlayerSpeedChargeWeapon += 0.5f;
        if (Game.Instance.Player.PlayerSpeedChargeWeapon >= 2f) Game.Instance.Player.PlayerSpeedChargeWeapon = 2f;

        _nextLevelText.gameObject.SetActive(true);
        _buttonNext.gameObject.SetActive(true);
        _bonusPanelGO.gameObject.SetActive(false);

        SaveLoadManager.MakeSave();
    }

    private void StartLevel()
    {
        Game.Instance.LevelsManager.StartLevel();
        HideMainMenu();
    }

    public void ShowWinUi()
    {
        Game.Instance.LevelsManager.NextLevel();
        _nextLevelText.text = "Level " + Game.Instance.LevelsManager.NomerLevel;
        _winUI.SetActive(true);

        int countBtns = 0;
        if (Game.Instance.Player.PlayerCountShields < 3)
        {
            _buttonAddShield.gameObject.SetActive(true);
            countBtns++;
        }
        else
            _buttonAddShield.gameObject.SetActive(false);

        if (Game.Instance.Player.PlayerSpeedChargeWeapon <= 1.5f)
        {
            _buttonAddSpeed.gameObject.SetActive(true);
            countBtns++;
        }
        else
            _buttonAddSpeed.gameObject.SetActive(false);

        if (countBtns == 2)
            _orText.gameObject.SetActive(true);
        else
            _orText.gameObject.SetActive(false);

        if (countBtns == 0)
        {
            _bonusPanelGO.gameObject.SetActive(false);
            _nextLevelText.gameObject.SetActive(true);
            _buttonNext.gameObject.SetActive(true);
        }
        else
        {
            _bonusPanelGO.gameObject.SetActive(true);
            _nextLevelText.gameObject.SetActive(false);
            _buttonNext.gameObject.SetActive(false);
        }


        SaveLoadManager.MakeSave();
    }

    private void ShowLevelTexts()
    {
        _explanationTextGO.SetActive(true);
        _levelTextGO.SetActive(true);
        _levelText.text = "Level " + Game.Instance.LevelsManager.NomerLevel;
    }



}
