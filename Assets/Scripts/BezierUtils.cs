using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BezierUtils
{
    private static Vector3[] _positionsV3;
    public static Vector3[] MakeBezierPoints(Vector3 startPosition, Vector3 endPosition, Vector3 startTangent, Vector3 endTangent, int division)
    {
        if (division > 1)
        {
            _positionsV3 = new Vector3[division];
            for (int i = 0; i < division; i++)
            {
                float t = i / (float)(division - 1);
                _positionsV3[i] = CalculateCubicBezierPoint(t, startPosition, startTangent, endTangent, endPosition);
            }
            return _positionsV3;
        }
        else
            return new Vector3[1] { startPosition };
    }

    private static Vector3 CalculateCubicBezierPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * p0;
        p += 3 * uu * t * p1;
        p += 3 * u * tt * p2;
        p += ttt * p3;

        return p;
    }
}
