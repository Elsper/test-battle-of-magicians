using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Wizard
{
    private int _playerCountShields = 0;
    private float _playerSpeedChargeWeapon = 1;
    
    public int PlayerCountShields
    {
        get { return _playerCountShields; }
        set { _playerCountShields = value; }
    }
    public float PlayerSpeedChargeWeapon
    {
        get { return _playerSpeedChargeWeapon; }
        set { _playerSpeedChargeWeapon = value; }
    }

    public override void WakeUpWhenStartLevel(int level)
    {
        WakeUP(Game.Instance.Player.PlayerSpeedChargeWeapon, Game.Instance.Player.PlayerCountShields);
    }
}
