using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelsManager
{
    private int _level = 1;
    private int _maxLevel = 8;

    public int NextLevel()
    {
        _level++;
        if (_level > _maxLevel) _level = _maxLevel;
        return _level;
    }
    public int NomerLevel
    {
        get { return _level; }
        set { _level = value; }
    }

    public event Action OnEndLevel;
    public event Action<int> OnStartLevel;
    public void EndLevel() => OnEndLevel?.Invoke();
    public void StartLevel() => OnStartLevel?.Invoke(_level);

}
