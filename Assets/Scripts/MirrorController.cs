using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorController : MonoBehaviour
{
    [SerializeField] private List<GameObject> _mirrors;

    public void Init()
    {
        Game.Instance.LevelsManager.OnStartLevel += OnInitLevel;

        foreach (GameObject item in _mirrors)
        {
            item.name = "Plane";
        }
    }

    private void OnDestroy()
    {
        Game.Instance.LevelsManager.OnStartLevel -= OnInitLevel;
    }
    private void OnInitLevel(int level)
    {
        for (int i = 0; i < _mirrors.Count; i++)
        {
            if (level > i) _mirrors[i].SetActive(true); else _mirrors[i].SetActive(false);
        }
    }
}
