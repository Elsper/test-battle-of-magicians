using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{
    [SerializeField] public Transform CameraTergetPointFollow;
    [SerializeField] public Transform CameraTergetPointAim;

    public void Init()
    {
        SetCameraByDefault();

        Game.Instance.LevelsManager.OnStartLevel += StartCameraMotion;
        Game.Instance.LevelsManager.OnEndLevel += SetCameraByDefault;
    }
    private void OnDestroy()
    {
        Game.Instance.LevelsManager.OnStartLevel -= StartCameraMotion;
        Game.Instance.LevelsManager.OnEndLevel -= SetCameraByDefault;
    }
    public void SetCameraByDefault()
    {
        CameraTergetPointFollow.transform.DOKill();
        CameraTergetPointAim.transform.DOKill();

        CameraTergetPointAim.transform.DOLocalMove(new Vector3(0, -200, -5), 1f);
        CameraTergetPointFollow.transform.DOLocalMove(new Vector3(0, 0, -60), 1f);
    }

    public void StartCameraMotion(int level = 0)
    {
        CameraTergetPointFollow.transform.DOLocalMove(new Vector3(32.5f, -72.4f, -26.2f), 3f);
        CameraTergetPointAim.transform.DOLocalMove(new Vector3(-39.4f, -110f, -27.5f), 3f);
    }

}
