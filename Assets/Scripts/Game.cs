using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUpdate
{
    void Update();
}

public class Game : MonoBehaviour
{
    public static Game Instance;
    public List<IUpdate> _updateSystems = new List<IUpdate>();

    [SerializeField] private CameraController CameraController;
    [SerializeField] private MirrorController MirrorController;
    [SerializeField] public UIManager UIManager;

    public LevelsManager LevelsManager;
    public BattleController BattleController;
    public Player Player;
    public Enemy Enemy;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;

        LevelsManager = new LevelsManager();
        BattleController = new BattleController();
        _updateSystems.Add(BattleController);

        CameraController.Init();
        MirrorController.Init();
        UIManager.Init();
        Player.Init();
        Enemy.Init();

        Player.SetIsPLayer = true;
    }

    void Update()
    {
        _updateSystems.ForEach(s => s.Update());
    }

}
