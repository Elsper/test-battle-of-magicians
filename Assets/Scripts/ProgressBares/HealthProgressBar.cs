using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthProgressBar : ProgressBar
{
    public float MakeDamage(float damage)
    {
        return ChangeHealth(-damage);
    }

    public float MakeHeal(float heal)
    {
        return ChangeHealth(heal);
    }

    private float ChangeHealth(float deltaHealth)
    {
        CurrentProgress += deltaHealth;
        if (CurrentProgress < 0) CurrentProgress = 0;
        if (CurrentProgress > MaxProgress) CurrentProgress = MaxProgress;
        UpdateProgress();

        return CurrentProgress;
    }

    public override void UpdateProgress()
    {
        base.UpdateProgress();
        float fillAmount = CurrentProgress / MaxProgress;
        GetFillImage.fillAmount = fillAmount;

        float g;
        float r;

        if (fillAmount >= 0.5 && fillAmount <= 1)
        {
            r = (1 - fillAmount) / 0.5f;
            GetFillImage.color = new Color(r, 1.0f, 0.0f);

        }
        else if (fillAmount >= 0 && fillAmount < 0.5)
        {
            g = fillAmount / 0.5f;
            GetFillImage.color = new Color(1.0f, g, 0.0f);
        }
    }
}
