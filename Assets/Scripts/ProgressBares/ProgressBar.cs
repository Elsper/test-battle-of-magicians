﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField] Image _fillImage;
    [SerializeField] TextMeshProUGUI _fillText;

    private float _speedRegen;
    private bool _autoRegen = false;

    private float _currentProgress = 0;
    private float _maxProgress = 100;

    public event Action OnFullProgress;


    public float CurrentProgress
    {
        get { return _currentProgress; }
        set
        {
            _currentProgress = value;
            if (_currentProgress > _maxProgress)
                _currentProgress = _maxProgress;

            UpdateProgress();
        }
    }

    public float MaxProgress
    {
        get { return _maxProgress; }
        set { _maxProgress = value; }
    }

    public Image GetFillImage
    {
        get { return _fillImage; }
    }

    public virtual void UpdateProgress()
    {
        float fillAmount = _currentProgress / _maxProgress;
        _fillImage.fillAmount = fillAmount;

        if (_fillText != null)
            _fillText.text = ((int)(_maxProgress * fillAmount)).ToString();
    }

    public void MakeAutoRegen(float speed)
    {
        _speedRegen = speed;
        _autoRegen = true;
    }

    private void Update()
    {
        if (_autoRegen)
        {
            if (_currentProgress < _maxProgress)
            {
                _currentProgress += Time.deltaTime * _speedRegen * _maxProgress;

                if (_currentProgress >= _maxProgress)
                {
                    _currentProgress = _maxProgress;
                    OnFullProgress?.Invoke();
                }

                UpdateProgress();
            }
        }
    }





}
