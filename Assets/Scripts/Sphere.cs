using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Sphere : MonoBehaviour
{
    private float _lifeTime = 5.1f;
    private float _speed = 10;
    private float _damage = 10;
    private bool _player;

    private bool _ReadyToDestroy = false;

    public void StartShoot(float speed, float damage, List<Vector3> traceShoot, bool player)
    {
        _player = player;
        _speed = speed;
        _damage = damage;

        gameObject.transform.DOPath(traceShoot.ToArray(), 2);
        StartCoroutine(TryDestroy());
    }

    IEnumerator TryDestroy()
    {
        while (!_ReadyToDestroy)
        {
            _ReadyToDestroy = true;
            yield return new WaitForSeconds(_lifeTime);
        }
        Destroy(gameObject);
        yield break;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Shield shield))
        {
            if (!shield.IsSameOwner(_player))
            {
                Destroy(gameObject);
                return;
            }
        }

        if (other.TryGetComponent(out Wizard wizard))
        {
            wizard.TakeDamage(_damage);
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        gameObject.transform.DOKill();
    }

}
